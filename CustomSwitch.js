import React from "react";
import { View, Switch } from "react-native";
import styles from "./styles";

export default function CustomSwich(props) {
  return (
    <View>
      <Switch
        trackColor={{ false: "grey", true: "black" }}
        thumbColor={"purple"}
        ios_backgroundColor="lightgray"
        onValueChange={props.toggleSwitch}
        value={props.isEnabled}
      />
    </View>
  );
}
