import { useNavigation } from "@react-navigation/native";
import React from "react";
import { View, Text } from "react-native";
import { Button } from "react-native-elements";

function HomeScreen() {
  const navigation = useNavigation();

  return (
    <View>
      <Text>Home Screen</Text>
      <Button
        title={"Go to Product Category"}
        onPress={() => navigation.navigate("ProductCategory")}
      />
    </View>
  );
}

export default HomeScreen;
