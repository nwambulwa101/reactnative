import React from "react";
import { TouchableOpacity, View, Text } from "react-native";
import styles from "./styles";

export default function CustomButton(props) {
  return (
    <View>
      <TouchableOpacity style={styles.button} onPress={props.func}>
        <Text style={styles.buttontext}>{props.name}</Text>
      </TouchableOpacity>
    </View>
  );
}
