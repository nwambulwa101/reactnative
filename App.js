import { NavigationContainer } from "@react-navigation/native";
import styles from "./styles";
import { View, Text } from "react-native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HomeScreen from "./HomeScreen";
import ProductCategory from "./ProductCategory";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <View style={styles.container}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="ProductCategory" component={ProductCategory} />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
}
