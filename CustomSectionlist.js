import React from "react";
import { SectionList, View, Text } from "react-native";
import styles from "./styles";

export default function CustomSectionlist(props) {
  const Item = ({ title }) => (
    <View style={styles.text}>
      <Text>{title}</Text>
    </View>
  );

  return (
    <View>
      <SectionList
        sections={props.data}
        keyExtractor={(item, index) => item + index}
        renderItem={({ item }) => <Item title={item} />}
        renderSectionHeader={({ section: { title } }) => (
          <Text style={styles.text2}>{title}</Text>
        )}
      />
    </View>
  );
}
