import React, { useState } from "react";
import { TextInput, TouchableOpacity, View, Text, Modal } from "react-native";
import styles from "./styles";

export default function Greeting() {
  const [name, setName] = useState("");
  const [open, setOpen] = useState(false);
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Name"
        onChangeText={(text) => setName(text)}
      />
      <TouchableOpacity onPress={() => setOpen(true)} style={styles.button}>
        <Text style={styles.text}>Submit</Text>
      </TouchableOpacity>
      <Modal visible={open}>
        <Text style={styles.text2}>Greetings, {name}!</Text>
        <TouchableOpacity onPress={() => setOpen(false)} style={styles.button}>
          <Text style={styles.text}>Close</Text>
        </TouchableOpacity>
      </Modal>
    </View>
  );
}
